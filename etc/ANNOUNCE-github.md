# The Mathematical Components github organization

We are plase to announce that the development of the Mathematical Components now
happens in the public and takes advantage of github's organization system.

http://math-comp.github.io/math-comp/

The purpose of such organization is to create a synergy between the Mathematical Components
library and its ecosystem of related projects.

The main archive is now avilable as a git repository in the organization name space
https://github.com/math-comp/math-comp and accepts bug reports and pull requests via
the standard github interface.

Users of the library are encouraged to join the organization and place (or mirror) their projects
in the organization name space.  The immediate gain is discoverability.  The more long term deal
we propose is that the impact of non backward compatibile changes in the Mathematical Components
library will be carefully assessed on all the mature projects part of the organization.  In
exchange we expect members of the organization to actively maintain their code when a breaking
change is applied or a patch is proposed.

The organization hosts a wiki for exchanging tips and tricks, gotchas, good practices, etc:
https://github.com/math-comp/math-comp/wiki

The ssreflect mailing list shall serve as the privileged communication channel between the
organization members: https://sympa.inria.fr/sympa/info/ssreflect

-- The Mathematical Components team
